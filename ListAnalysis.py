class palindromo:
    def __init__(self, palabra):
        #  palabra = raw_input('Ingrese la palabra:')
        self.palabra = palabra

    def esPalindromo(self):
        lista = list(self.palabra)
        listaReverse = [lista[i - 1] for i in range(len(lista), 0, -1)]

        if lista == listaReverse:
            print 'la palabra %s es un palindromo' % self.palabra
        else:
            print 'la palabra %s NO lo es' % self.palabra


# Testcases

pal1 = palindromo("Juanelo")
pal1.esPalindromo()

pal1 = palindromo("Oso ana osO")
pal1.esPalindromo()

pal1 = palindromo("AbCcBa")
pal1.esPalindromo()

pal1 = palindromo("122333221")
pal1.esPalindromo()

pal1 = palindromo("99 88")
pal1.esPalindromo()

pal1 = palindromo("")
pal1.esPalindromo()
